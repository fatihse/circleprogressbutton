# README #

![ezgif.com-optimize.gif](https://bitbucket.org/repo/ppb9Kp/images/2721588965-ezgif.com-optimize.gif)

### What is this repository for? ###

* View in circle shape that can be used as button with progress on long click.
Also as static view to show progress or to simulate progress animation.
* last version 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Please test the example app that showcase some of the possibilities of the library. There is an example how to set the view in activity and xml. Also how
to use it as a button with long click, as static view just to show a progress and with progress simulation. 

* Configuration

If you don't use jcenter() add
```
#!java
repositories {
   maven {

        url 'https://dl.bintray.com/galeen/maven/'
    }
}

```
in your application gradle

* Dependencies

` compile 'com.criapp.ui:circleprogressbutton:0.0.1'`

### How to use ###
Simplest way to add a view in xml and to be configured in the code 
```
#!java

<com.criapp.circleprogresscustomview.CircleProgressView
                android:id="@+id/vButton"
                android:layout_width="120dp"
                android:layout_height="120dp"/>
```
In the code set a callback and you are done!

```
#!java

 longClickBtn.setmCallback(new CircleProgressView.CircleProgressCallback() {
            @Override
            public void onProgress(int progress1) {
                tvProgress.setText(Integer.toString(progress1)+ "%");
            }

            @Override
            public void onFinish(int progress1) {
                tvProgress.setText(Integer.toString(progress1)+ "%-finished");
            }

            @Override
            public void onError(int progress1) {
                tvProgress.setText(Integer.toString(progress1)+"%-error");
            }
        });
```

### Customize ###
If you want to customize in code

```
#!java
        CircleProgressView iv = (CircleProgressView) findViewById(R.id.vButton);
        iv.setTopProgress(360);// full circle progress
        iv.setColorProgress(ContextCompat.getColor(this, color));//progress color
        iv.setColorDone(Color.GREEN);//color filled when done
        iv.setColorWrong(Color.RED);//color filled on error
        iv.setColorBaseFill(Color.GREY);//color filled while on progress
        iv.setColorBaseStroke(Color.GREY);//line color under the progress one
        iv.setColorWrongBaseStroke(Color.WHITE);//line color on error
        iv.setStrokeWidth(5); // the progress line width in px
        iv.setStartProgress(90);//will start from bottom, the default is -90 top
         //and many more options
```
in xml

```
#!java

...
xmlns:app="http://schemas.android.com/apk/res-auto"
...
 <com.criapp.circleprogresscustomview.CircleProgressView
                android:id="@+id/vButtonLap3"
                android:layout_width="75dp"
                android:layout_height="75dp"
                app:colorProgress="@color/blue"
                app:colorDone="@color/green_done"
                app:colorBaseStroke="@color/grey"
                app:colorBaseFill="@android:color/transparent"
                app:fullProgressPosition="360"
                app:shouldInit="false" // important if you want to just show static progress without on long click event, otherwise should be true
                app:startProgressPosition="-90"
                app:progress="69"/>
```
Full xml attributes

```
#!java

<declare-styleable name="CircleProgressView">
        <attr name="colorProgress" format="color" />//progress color
        <attr name="colorDone" format="color" />//color filled when done
        <attr name="colorWrong" format="color" />//color filled on error
        <attr name="colorBaseFill" format="color" />//color filled while on progress
        <attr name="colorBaseStroke" format="color" />//line color under the progress line
        <attr name="colorWrongStroke" format="color" />//line color on error
        <attr name="progress" format="integer" />//current progress default is 0
        <attr name="strokeWidth" format="integer" />// the progress line width in px default is 6
        <attr name="progressPeriodMills" format="integer" />//mills between each update default is 30
        <attr name="startProgressPosition" format="integer" />//position from where to start the progress default is -90 which is top 12 o'clock, 0 is 3 o'clock, 90 is 6 and so on 
        <attr name="fullProgressPosition" format="integer" />// where the prorgess to stop default is 360, full ring. 
//If is 180 will return 100% and stop by half-moon :)
        <attr name="style" format="integer" />// CIRCLE=2, CLOCK=1, FILLED_CIRCLE=0 default is 2
        <attr name="shouldInit" format="boolean" />//if you show static progress set to false if you use it as a button set to true default is true
        <attr name="isInError" format="boolean" />//set the view in error mode default is false
    </declare-styleable>
```
 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* 2math(a)mail.bg
* Other community or team contact